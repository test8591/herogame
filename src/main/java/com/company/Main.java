package com.company;

import com.company.characters.*;
import com.company.item.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<com.company.characters.Char> characters = new ArrayList<>();
        Char mage = new Mage("Mage");
        Char ranger = new Ranger("Ranger");
        Char rogue = new Rogue("Rogue");
        Char warrior = new Mage("Warrior");
        characters.add(mage);
        characters.add(ranger);
        characters.add(rogue);
        characters.add(warrior);

        Weapon staff = new Weapon(WeaponType.STAFF, "myStaff", 1, 10, 0.2);
        mage.setEquipment(staff);
//        Armor plateBody = new Armor(ArmorType.CLOTH, Slot.BODY, "PLATTA", 8);
            Armor plateBody = new Armor(ArmorType.CLOTH, Slot.BODY, "Cloth", 1, 2, 4, 1, 1, 1);
//        Armor plateLegs = new Armor(ArmorType.PLATE, Slot.LEGS, "PLATTA", 8);
        mage.setEquipment(plateBody);
//        mage.setEquipment(plateLegs);

        System.out.println(staff.getDPS());
//        System.out.println(mage.getInitialAttributes().getIntelligence());

        /*
        weapon.setSlot(Slot.WEAPON);
        weapon.setWeaponType(WeaponType.AXE);
        weapon.setDamage(7);
        weapon.setAttackSpeed(1.1);
        */


        for (Char c : characters) {
            System.out.println("***************************************");
            System.out.println(c.toString());
        }
        System.out.println("***************************************");


/*
        Character hero = new Character("test", 1);
        System.out.println("Name: " + hero.getName());
        System.out.println("Level: " + hero.getLevel());

        ArrayList<Character> characters = new ArrayList<Character>();
        characters.add(0, hero);
        hero = new Character("test2", 2);
        characters.add(1, hero);
        hero = new Character("test3", 3);
        characters.add(2, hero);
*/
    }
}
