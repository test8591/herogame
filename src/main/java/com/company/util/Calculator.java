package com.company.util;

import com.company.Attributes;

public class Calculator {

    public static int getDexterity(Attributes initialAttributes, Attributes levelAttributes, int level) {
        return initialAttributes.getDexterity() + (level -1) * levelAttributes.getDexterity();
    }

    public static int getIntelligence(Attributes initialAttributes, Attributes levelAttributes, int level) {
        return initialAttributes.getIntelligence() + (level -1) * levelAttributes.getIntelligence();
    }

    public static int getStrength(Attributes initialAttributes, Attributes levelAttributes, int level) {
        return initialAttributes.getStrength() + (level -1) * levelAttributes.getStrength();
    }
}
