package com.company.item;

import com.company.Attributes;
import com.company.util.Calculator;

public class Armor extends Equipment {
    private final ArmorType armorType;
    private final Attributes initialAttributes;
    private final Attributes levelAttributes;

    public Armor(ArmorType type, Slot slot, String name, int strength, int dexterity, int intelligence, int strengthPrLevel, int dexterityPrLevel, int intelligencePrLevel) {
        super(slot, name, 1);
        this.initialAttributes = new Attributes(strength, dexterity, intelligence) ;
        this.levelAttributes = new Attributes(strengthPrLevel, dexterityPrLevel, intelligencePrLevel) ;
        this.armorType = type;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public int getStrength() {
        return Calculator.getStrength(initialAttributes, levelAttributes, getLevel());
    }

    public int getDexterity() {
        return Calculator.getDexterity(initialAttributes, levelAttributes, getLevel());
    }

    public int getIntelligence() {
        return Calculator.getIntelligence(initialAttributes, levelAttributes, getLevel());
    }

    public double getDPS() {
        return getStrength() + getDexterity() + getIntelligence();
    }

    public String toString() {
        return getArmorType().toString();
    }
}
