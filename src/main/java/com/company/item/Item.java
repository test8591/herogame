package com.company.item;

public abstract class Item {
    Slot type;
    public Item(Slot type) {
        this.type = type;
    }

    public Slot getItemType() {
        return type;
    }


}
