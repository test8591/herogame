package com.company.item;

public class Weapon extends Equipment {
    WeaponType weaponType;
    int damage;
    double attackSpeed;

    public Weapon(WeaponType weaponType, String name, int level, int damage, double attackSpeed) {
        super(Slot.WEAPON,name, level);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;

    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public double getDPS() {
        return damage * attackSpeed;
    }

    public void setDamage(Integer damage) {
        this.damage = damage;
    }

    public Double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(Double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public String toString() {
        return getWeaponType().toString() + " ("  + damage + ", " + attackSpeed + ")";
    }
}
