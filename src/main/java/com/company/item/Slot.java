package com.company.item;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
