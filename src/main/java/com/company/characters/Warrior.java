package com.company.characters;

import com.company.Attributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Warrior  extends Char {
    public Warrior(String name) {
        super(name, 1);
        getLegalWeapons().add(WeaponType.AXE);
        getLegalWeapons().add(WeaponType.HAMMER);
        getLegalWeapons().add(WeaponType.SWORD);
        getLegalArmors().add(ArmorType.PLATE);
        getLegalArmors().add(ArmorType.MAIL);
        setInitialAttributes(new Attributes(5,2,1));
        setLevelAttributes(new Attributes(3, 2, 1));
    }
}
