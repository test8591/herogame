package com.company.characters;

import com.company.Attributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Ranger extends Char {
    public Ranger(String name) {
        super(name, 1);
        getLegalWeapons().add(WeaponType.BOW);
        getLegalArmors().add(ArmorType.LEATHER);
        getLegalArmors().add(ArmorType.MAIL);
        setInitialAttributes(new Attributes(1,7,1));
        setLevelAttributes(new Attributes(1,5,1));
    }
}
