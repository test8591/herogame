package com.company.characters;

import com.company.Attributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Mage extends Char {
    public Mage(String name) {
        super(name, 1);
        getLegalWeapons().add(WeaponType.STAFF);
        getLegalWeapons().add(WeaponType.WAND);
        getLegalArmors().add(ArmorType.CLOTH);
        setInitialAttributes(new Attributes(1, 1, 8));
        setLevelAttributes(new Attributes(1, 1, 5));
    }
}
