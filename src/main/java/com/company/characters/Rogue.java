package com.company.characters;

import com.company.Attributes;
import com.company.item.ArmorType;
import com.company.item.WeaponType;

public class Rogue extends Char {
    public Rogue(String name) {
        super(name, 1);
        getLegalWeapons().add(WeaponType.DAGGER);
        getLegalWeapons().add(WeaponType.SWORD);
        getLegalArmors().add(ArmorType.LEATHER);
        getLegalArmors().add(ArmorType.MAIL);
        setInitialAttributes(new Attributes(2, 6, 1));
        setLevelAttributes(new Attributes(1, 4, 1));
    }
}
