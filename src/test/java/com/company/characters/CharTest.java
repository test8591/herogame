package com.company.characters;

import com.company.Attributes;
import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CharTest {
    //triks for å kunne teste en abstrakt klasse
    private static class CharX extends Char {
        public CharX(String name) {
            super(name, 1);
        }
    }

//    @Test
    void testLevelAdjustment() {
        Char c = new CharX("")
                .setInitialAttributes(new Attributes(1, 2, 8))
                .setLevelAttributes(new Attributes(1, 1, 5));
        assertEquals(1, c.getLevel());
        assertEquals(1, c.getStrength());
        assertEquals(2, c.getDexterity());
        assertEquals(8, c.getIntelligence());
        c.setLevel(2);
        assertEquals(2, c.getLevel());
        assertEquals(2, c.getStrength());
        assertEquals(3, c.getDexterity());
        assertEquals(13, c.getIntelligence());
    }

//    @Test
    void testEquipment() throws InvalidWeaponException, InvalidArmorException {
        Char c = new CharX("");
        assertNull(c.getEquipment(Slot.BODY));
        assertNull(c.getEquipment(Slot.WEAPON));
        c.setEquipment(new Weapon( WeaponType.AXE, "", 1, 12, 0.8));
        assertNull(c.getEquipment(Slot.BODY));
        assertNotNull(c.getEquipment(Slot.WEAPON));
        c.setEquipment(new Weapon(WeaponType.BOW, "", 1, 12, 0.8));
        assertNull(c.getEquipment(Slot.BODY));
        assertNotNull(c.getEquipment(Slot.WEAPON));
    }



    // Unit testing and test coverage
    // 1
    @Test
    void testCharacterLevel1WhenCreated() {
        Char mage = new Mage("");
        assertEquals(1, mage.getLevel());
    }

    // 2
    @Test
    void testCharacterGainLevelAtStart() {
        Char mage = new Mage("");
        mage.setLevel(2);
        assertEquals(2, mage.getLevel());
    }

    // 3
    @Test
    void testDefaultCharacterValues() {
        testDefaultCharacterValues(new Mage(""), 1,1,8,1,1,5);
        testDefaultCharacterValues(new Ranger(""),1,7,1,1,5,1);
        testDefaultCharacterValues(new Rogue(""),2,6,1,1,4,1);
        testDefaultCharacterValues(new Warrior(""),5,2,1,3,2,1);
    }
    void testDefaultCharacterValues(Char character, int st, int de, int in, int stl, int del, int inl) {
        assertEquals(st, character.getInitialAttributes().getStrength(), "Strength level wrong");
        assertEquals(de, character.getInitialAttributes().getDexterity(), "Dexterity level wrong");
        assertEquals(in, character.getInitialAttributes().getIntelligence(), "Intelligence level wrong");
        assertEquals(stl, character.getLevelAttributes().getStrength(), "Strength adjustment level wrong");
        assertEquals(del, character.getLevelAttributes().getDexterity(), "Dexterity adjustment level wrong");
        assertEquals(inl, character.getLevelAttributes().getIntelligence(), "Intelligence adjustment level wrong");
    }

    // 4
    @Test
    void testCharacterGainLevelAtStar99999t() {
        testCharacterAttributesWhenLevelUp(new Mage(""),2,2,13);
        testCharacterAttributesWhenLevelUp(new Ranger(""),2,12,2);
        testCharacterAttributesWhenLevelUp(new Rogue(""),3,10,2);
        testCharacterAttributesWhenLevelUp(new Warrior(""), 8, 4, 2);
    }

    void testCharacterAttributesWhenLevelUp(Char character, int strength, int dexterity, int intelligence) {
        character.setLevel(2);
        assertEquals(strength, character.getStrength(), "Strength level up is wrong");
        assertEquals(dexterity, character.getDexterity(), "Dexterity level up is wrong");
        assertEquals(intelligence, character.getIntelligence(), "Intelligence level up is wrong");
    }

    // Items and equipment tests
    // 1
    // If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
    // Use the warrior, and the axe, but set the axes level to 2
    @Test
    void testWeaponWithTooHighLevel() {
        Char warrior = new Warrior("");
        Weapon axe = new Weapon(WeaponType.AXE, "", 2, 10, 0.2);
        assertThrows(InvalidWeaponException.class, () -> {
            warrior.setEquipment(axe);
        });
    }

    // 2
    // If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
    //  Use the warrior, and the plate body armor, but set the armor’s level to 2.
    @Test
    void testArmorWithTooHighLevel() {
        Char warrior = new Warrior("");
        Armor platebody = new Armor(ArmorType.PLATE, Slot.BODY, "", 2, 2, 2, 1, 1, 1);
        platebody.setLevel(2);
        assertThrows(InvalidArmorException.class, () -> {
            warrior.setEquipment(platebody);
        });
    }

    // 3
    // If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
    // Use the warrior and the bow.
    @Test
    void testCharacterEquipWrongWeaponType() {
        Char warrior = new Warrior("");
        Weapon bow = new Weapon(WeaponType.BOW, "", 1, 10, 0.2);
        assertThrows(InvalidWeaponException.class, () -> {
            warrior.setEquipment(bow);
        });
    }

    // 4
    // If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
    // Use the warrior and the cloth armor.
    @Test
    void testCharacterEquipWrongArmorType() {
        Char warrior = new Warrior("");
        Armor cloth = new Armor(ArmorType.CLOTH, Slot.BODY, "", 2, 2, 2, 1, 1, 1);
        assertThrows(InvalidArmorException.class, () -> {
            warrior.setEquipment(cloth);
        });
    }

    // 5
    // If a character equips a valid weapon, a Boolean true should be returned.
    @Test
    void testCharacterEquipValidWeapon() {
        Char warrior = new Warrior("");
        Weapon axe = new Weapon(WeaponType.AXE, "", 1, 10, 0.2);
        assertTrue(warrior.setEquipment(axe));
    }

    // 6
    // If a character equips a valid armor piece, a Boolean true should be returned.
    @Test
    void testCharacterEquipValidArmor() {
        Char warrior = new Warrior("");
        Armor plate = new Armor(ArmorType.PLATE, Slot.BODY, "", 2, 2, 2, 1, 1, 1);
        assertTrue(warrior.setEquipment(plate));
    }

    // 7
    // Calculate DPS if no weapon is equipped.
    // Take warrior at level 1
    // Expected DPS = 1*(1 + (5 / 100))
    @Test
    void testCalculateDPSIfNoWeaponEquipped() {
        Char warrior = new Warrior("");
        assertEquals(1.11, warrior.getDPS()); //spør foreleser om korrekt beregning
        warrior.setLevel(2);
        assertEquals(1.17, warrior.getDPS());
    }

    // 8
    // Calculate DPS with valid weapon equipped.
    // Take warrior level 1.
    // Equip axe.
    // Expected DPS = (7 * 1.1)*(1 + (5 / 100))
    @Test
    void testCalculateDPSWithValidWeapon() {
        Char warrior = new Warrior("");
        Weapon axe = new Weapon(WeaponType.AXE, "", 1, 7, 1.1); // spør foreleser om attributter på våpen
        warrior.setEquipment(axe);
        assertEquals(8.547000000000002, warrior.getDPS());
    }

    // 9
    // Calculate DPS with valid weapon and armor equipped.
    // Take warrior level 1.
    // Equip axe.
    // Equip plate body armor.
    // Expected DPS = (7 * 1.1) * (1 + ((5+1) / 100))
    @Test
    void testCalculateDPSWithValidWeaponAndArmorEquipped() {
        Char warrior = new Warrior("");
        Weapon axe = new Weapon(WeaponType.AXE, "", 1, 7, 1.1); // spør foreleser om attributter på våpen
        warrior.setEquipment(axe);
        Armor plate = new Armor(ArmorType.PLATE, Slot.BODY, "", 2, 2, 2, 1, 1, 1);
        warrior.setEquipment(plate);
        assertEquals(8.932, warrior.getDPS());
    }
}
